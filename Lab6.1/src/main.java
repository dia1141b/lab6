/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author dia1141b
 */
public class main {

    public static void main(String[] args) {
        Pegas p1 = new Pegas("Pegas");
        p1.air();
        Dog d1 = new Dog();
        d1.setName("Pes");
        d1.setAge(11);
        d1.setVes(12);
        d1.setPol("m");
        d1.showInfoDog();
        Cat c1 = new Cat();
        c1.setName("KOT");
        c1.setAge(2);
        c1.setVes(7);
        c1.setPol("w");
        c1.showInfoCat();

        boolean yes = true;//Да
        boolean no = false;//Нет
        Animal a1 = new Animal(12, "Вода");
        Fish f1 = new Fish(3, "Озеро", no);
        Fish f2 = new Fish(50, "Море", no);
        f1.fishKtoKogo(f2);
        Ape ap1 = new Ape(30, "Дерево", "Олег", 153);
        Ape ap2 = new Ape(100, "Иследовательская лабаратория", "Святогор", 190);
        ap1.apeBitva(ap2);
        Human h1 = new Human(90, "Квартира", "Атос", 170, "Мушкеторов", 29, yes);
        Human h2 = new Human(120, "Спортзал", "Вениамин", 150, "Шариков", 45, no);
        h1.humanKulachiiBoy(h2);
        f2.fishVShuman(h2);
        
    }
}
